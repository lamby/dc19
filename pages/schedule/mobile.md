---
name: Mobile-friendly Schedule
---
# Mobile-friendly Schedule

The Schedule is available through an XML feed.
You can use _ConfClerk_ in Debian to consume this, or _Giggity_ on
Android.

<img class="img-fluid" src="{% static "img/dc19-schedule-qr-code.png" %}"
     title="DebConf19 Schedule">

XML Feed: [https://debconf19.debconf.org/schedule/pentabarf.xml](/schedule/pentabarf.xml)

## Download Giggity

- Home page: [https://gaa.st/giggity](https://gaa.st/giggity)
- F-Droid: [https://f-droid.org/repository/browse/?fdid=net.gaast.giggity](https://f-droid.org/repository/browse/?fdid=net.gaast.giggity)
- Google Play Store: [https://play.google.com/store/apps/details?id=net.gaast.giggity
](https://play.google.com/store/apps/details?id=net.gaast.giggity)

## Download ConfClerk

	# apt install confclerk
