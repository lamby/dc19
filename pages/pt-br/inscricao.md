---
name: Informações sobre a inscrição
---
# Informações sobre a inscrição

A inscrição online para a DebConf19 começará no início de 2019. Entre
na lista de discussão debconf-announce para ser notificado(a) (em
Inglês) quando a inscrição para o evento começar:

<!-- Online registration for DebConf19 will open early in 2019. -->
<!-- Subscribe to debconf-announce to be notified when registration opens: -->

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Inscreva-se na lista de discussão debconf-announce</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

## Taxas de inscrição

Como sempre, a inscrição básica para a DebConf é de graça para os(as)
participantes.
Se você for participar do evento profissionalmente ou como um(a)
representante da sua empresa, nós pedimos para que você considere a
possibilidade de se inscrever em uma das nossas categorias pagas, para
ajudar a cobrir os custos da Conferência:

**Inscrição profissional**: com uma taxa de R$ 500,00 para a semana,
essa inscrição cobre os custos associados à sua participação
individual na Conferência.

**Inscrição corporativa**: com uma taxa de R$ 1.750,00, essa categoria
é feita para aqueles(a) que participarão da DebConf como
representantes de empresas, e ajuda a custear a Conferência para
outras pessoas.

Assim como nas três DebConfs anteriores, essas taxas de inscrição não
incluem alimentação ou hospedagem.

Nós encorajamos que todos(as) os(as) participantes ajudem a fazer da
DebConf um sucesso ao escolherem apropriadamente a categoria de
inscrição que mais se encaixa com seus perfis.

## Participação patrocinada

Qualquer membro da comunidade Debian pode solicitar bolsa (patrocínio)
para participar da DebConf.
Como em anos anteriores, nós faremos nosso melhor para oferecermos
auxílio com alimentação e hospedagem aos contribuidores Debian, mas a
oferta de patrocínio é sujeita à demanda e aos recursos financeiros
disponíveis. Decisões a respeito de solicitações de bolsa serão
divulgadas no final de abril de 2019.
