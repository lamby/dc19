---
name: Creche
---
# Creche

Durante a DebConf19, haverá uma sala na UTFPR para que
pais/mães que quiserem vir ao evento com crianças possam deixá-las gratuitamente
sob os cuidados de profissionais especializados.

Esta sala terá duas cuidadoras que ficarão encarregadas de entreter as crianças
enquanto seus pais/mães participam das atividades da DebConf19.

Este serviço será custeado pela organização do evento.
