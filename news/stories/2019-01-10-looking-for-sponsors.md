---
title: DebConf19 is looking for sponsors!
---

Leia em Português abaixo.

**[DebConf19][debconf19] will be held in
[Curitiba](/about/curitiba), Brazil from July 21th
to 28th, 2019**. 
It will be preceded by DebCamp, July 14th to 19th, 
and [Open Day](/schedule/openday/) on the 20th.

[DebConf][debconf], Debian's annual developers conference,
is an amazing event where Debian contributors from all around the world
gather to present, discuss and work in teams around the Debian operating
system. It is a great opportunity to get to know people responsible for the
success of the project and to witness a respectful and functional distributed
community in action.

The DebConf team aims to organize the Debian Conference
as a self-sustaining event, despite its size and complexity.
The financial contributions and support by individuals, companies and
organizations are pivotal to our success.

There are many different possibilities to support DebConf
and we are in the process of contacting potential sponsors 
from all around the globe.
If you know any organization that could be interested
or who would like to give back resources to FOSS,
please consider handing them
the [sponsorship brochure][brochure]
or [contact the fundraising team][sponsors] with any leads.
If you are a company and want to sponsor, please contact us at [sponsors@debconf.org][sponsors].

Let’s work together, as every year, on making the best DebConf ever.
We are waiting for you at Curitiba!

This news item was originally posted in [the Debian blog][blog].

*****

# DebConf19 procura patrocinadores!

**A [DebConf19][debconf19] vai acontecer em
[Curitiba](/about/curitiba), Brasil, de 21 a 28
de Julho de 2019**. Ela será precedida pelo DebCamp, de 14 a 19 de Julho,
e pelo [Open Day](/pt-br/openday/) no dia 20.

[DebConf][debconf], a conferência anual de desenvolvedores
e desenvolvedoras do Debian, é um evento incrível no qual contribuidores de
todo o mundo se encontram para apresentar, discutir e trabalhar em assuntos
relativos ao sistema operacional Debian. É uma ótima oportunidade para conhecer
as pessoas responsáveis pelo sucesso do projeto e para ver uma comunidade
distribuída em ação trabalhando de forma respeitosa e funcional.

O time da DebConf tem como objetivo organizar a Conferência Debian como um
evento auto-sustentável, apesar de seu tamanho e complexidade. As contribuições
financeiras e apoios de indivíduos, empresas e organizações são centrais para
nosso sucesso.

Existem muitas possibilidades diferentes de apoiar a DebConf e estamos em
processo de contactar potenciais patrocinadores ao redor do globo. Se você
conhece alguma organização que poderia se interessar ou querer retribuir de
volta ao mundo do Software Livre e Código Aberto, por favor envie a ela
o [folheto de patrocínio][brochure-pt]
ou [entre em contato com a equipe de patrocínio][sponsors]
para nos sugerir possíveis interessados. Se você for uma empresa e quiser
patrocinar, por favor entre em contato conosco através do e-mail
[sponsors@debconf.org][sponsors].

Vamos trabalhar juntos, como em todos os anos, para construir a melhor DebConf
de todas. Esperamos por vocês em Curitiba!

Esta notícia foi originalmente publicada no [blog do Debian][blog].

[debconf19]: https://debconf19.debconf.org
[debconf]: https://www.debconf.org
[brochure]: https://media.debconf.org/dc19/fundraising/debconf19_sponsorship_brochure_en.pdf
[brochure-pt]: https://media.debconf.org/dc19/fundraising/debconf19_sponsorship_brochure_pt_br.pdf
[sponsors]: mailto:sponsors@debconf.org
[blog]: https://bits.debian.org/2019/01/debconf19-looking-for-sponsors.html
